package Constants;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class Constants {
    public static final String P_NAME = "Product's Name : ";
    public static final String P_ID = "Product's ID : ";
    public static final String P_PRICE = "Product's Price : ";
    public static final String P_QTY = "Stock Quantity : ";
    public static  final String IS_UPDATE_RECORD = "Are you sure want to update this record ? [Y/y] or [N/n] : ";
    public static  final String IS_ADD_RECORD = "Are you sure want to add this record ? [Y/y] or [N/n] : ";
    public static  final String IS_DELETE_RECORD = "Are you sure want to delete this record ? [Y/y] or [N/n] : ";
}
