package Constants;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class MyTable {
    static  Table t = null;
    static Table theader = null;
    public static void info(String title){
        t = new Table(1, BorderStyle.DESIGN_CURTAIN_WIDE, ShownBorders.SURROUND);
        t.addCell(title);
        System.out.println(t.render());
    }
    public static void tableHeader(String header){
        theader = new Table(1, BorderStyle.UNICODE_BOX_HEAVY_BORDER);
        theader.setColumnWidth(0,70,0);
        theader.addCell(header);
        System.out.println(theader.render());
    }
}
