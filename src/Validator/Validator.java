package Validator;

import java.util.Scanner;

public class Validator {
    static public int getInteger(Scanner sc,String prompt){
        boolean isValid = false;
        int number = 0;
        while (isValid == false){
            System.out.print(prompt);
            if (sc.hasNextInt()){
                number = sc.nextInt();
                isValid = true;
            }else{
                System.out.println("Invalid number! ");
            }
            //next input
            sc.nextLine();
        }
        return number;
    }
    static public double getDouble(Scanner sc,String prompt){
        boolean isValid = false;
        double number = 0;
        while (isValid == false){
            System.out.print(prompt);
            if (sc.hasNextDouble()){
                number = sc.nextDouble();
                isValid = true;
            }else{
                System.out.println("Invalid number! ");
            }
            //next input
            sc.nextLine();
        }
        return number;
    }
}