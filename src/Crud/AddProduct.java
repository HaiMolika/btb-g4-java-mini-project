package Crud;

import Constants.*;
import Validator.Validator;
import database.ConnectionDB;
import homepage.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class AddProduct {
    static Connection con = null;
    static Statement stmt = null;
    static ResultSet rs = null;
    static Scanner sc = new Scanner(System.in);
    static LocalDate date = LocalDate.now();
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    static int id = 1;
    public static void addProduct(ArrayList<Product> products) throws SQLException {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        t.setColumnWidth(0,30,30);
        if(products.size() != 0){
            id = products.get(products.size()-1).getId() + 1;
        }
        System.out.println(Constants.P_ID +id);
        System.out.print(Constants.P_NAME);
        String name = sc.nextLine();
        double productPrice = Validator.getDouble(sc,Constants.P_PRICE);
        int qty = Validator.getInteger(sc,Constants.P_QTY);
        String importDate = date.format(formatter);
        t.addCell("ID          : " + id);
        t.addCell("Name        : " + name);
        t.addCell("Unit Price  : " + productPrice);
        t.addCell("Qty         : " + qty);
        t.addCell("Import Date : " + importDate);
        System.out.println(t.render());
        System.out.print(Constants.IS_ADD_RECORD);
        String isAdded = sc.nextLine();
        while(isAdded.equals("Y") || isAdded.equals("y")){
            products.add(new Product(id,name,productPrice,qty,importDate,"isAdded"));
            MyTable.info(id + " was added successfully");
            id +=1;
            return;
        }
        MyTable.info(id + " add fail! ");
    }

}
