package Crud;

import database.ConnectionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertProduct {
    static Connection con =null;
    static PreparedStatement preStmt = null;
    public static void insertProduct(){
        try {
            long start = System.currentTimeMillis();
            con = ConnectionDB.getConnection();
            preStmt = con.prepareStatement("delete from tblproduct");
            preStmt.executeUpdate();
            for (int i = 0; i < 10000000;i++){
                preStmt = con.prepareStatement("insert into tblproduct(\"name\",\"unitprice\",\"quantity\",\"importdate\") values('product',2.3,10,'10-10-2020')");
                preStmt.executeUpdate();
            }
            long end = System.currentTimeMillis();
            long diff = end - start;
            System.out.println("Total WRITE Time : "+ diff);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                con.close();
                preStmt.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
