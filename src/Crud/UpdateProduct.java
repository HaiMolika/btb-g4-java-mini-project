package Crud;

import Constants.*;
import homepage.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import Validator.Validator;

import java.util.ArrayList;
import java.util.Scanner;

public class UpdateProduct {
    static Scanner sc = new Scanner(System.in);
    public static void update(ArrayList<Product> list){

        int id = Validator.getInteger(sc,"Please Input ID of Product : ");
        int found = 0;

        for (Product product : list){
            if (product.getId() == id){
                found++;
                ReadProduct.read(list,id);
                System.out.println("What do you want to Update?");
                System.out.println();
                MyTable.tableHeader("1. All    2. Name    3. Quantity    4. Unit Price    5. Back To Menu");
                System.out.println();
                int option = Validator.getInteger(sc,"Option(1-5): ");
                A:
                switch (option){
                    case 1 :
                        updateAll(product);
                        break;
                    case 2:
                        updateName(product);
                        break;
                    case 3:
                        updateQuantity(product);
                        break;
                    case 4:
                        updateUnitPrice(product);
                        break;
                    case 5:
                        break A;
                    default:
                        System.out.println("Invalid option! ");
                }
            }
        }
        if (found == 0 ) {
            MyTable.info(id + " Product Not Found!");
            return;
        }
    }
    static public void updateAll(Product product){
        System.out.print(Constants.P_NAME);
        String productName = sc.nextLine();
        double productPrice = Validator.getDouble(sc,Constants.P_PRICE);
        int stockQty = Validator.getInteger(sc,Constants.P_QTY);
        System.out.print(Constants.IS_UPDATE_RECORD);
        String isUpdate = sc.nextLine();

        while(isUpdate.equals("Y") || isUpdate.equals("y")){
            product.setName(productName);
            product.setStockQty(stockQty);
            product.setUnitPrice(productPrice);
            product.setStatus("isUpdate");
            MyTable.info("Product was Updated");
            return;
        }
        MyTable.info("Product Not Update!");
    }

    static void updateName(Product product){

        System.out.print(Constants.P_NAME);
        String productName = sc.nextLine();
        System.out.print(Constants.IS_UPDATE_RECORD);
        String isUpdate = sc.nextLine();
        while (isUpdate.equals("Y") || isUpdate.equals("y")){
            product.setName(productName);
            product.setStatus("isUpdate");
            MyTable.info("Product was Updated");
            return;
        }
        MyTable.info("Product Not Update!");
    }
    static void updateQuantity(Product product){
        int stockQty = Validator.getInteger(sc,Constants.P_QTY);
        System.out.print(Constants.IS_UPDATE_RECORD);
        String isUpdate = sc.nextLine();
        while (isUpdate.equals("Y") || isUpdate.equals("y")){
            product.setStockQty(stockQty);
            product.setStatus("isUpdate");
            MyTable.info("Product was Updated");
            return;
        }
        MyTable.info("Product Not Update!");
    }
    static void updateUnitPrice(Product product){
        double stockQty = Validator.getDouble(sc,Constants.P_PRICE);
        System.out.print(Constants.IS_UPDATE_RECORD);
        String isUpdate = sc.nextLine();
        while (isUpdate.equals("Y") || isUpdate.equals("y")){
            product.setUnitPrice(stockQty);
            product.setStatus("isUpdate");
            MyTable.info("Product was Updated");
            return;
        }
        MyTable.info("Product Not Update!");
    }

}
