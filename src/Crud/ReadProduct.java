package Crud;

import Constants.MyTable;
import Validator.Validator;
import homepage.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.Scanner;

public class ReadProduct {
    public static void read(ArrayList<Product> products,int id){
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        t.setColumnWidth(0,30,30);

        int found = 0;

        for (Product product : products){
            if (product.getId() == id){
                found++;
                t.addCell("ID          : " + product.getId());
                t.addCell("Name        : " + product.getName());
                t.addCell("Unit Price  : " + product.getUnitPrice());
                t.addCell("Qty         : " + product.getStockQty());
                t.addCell("Import Date : " + product.getImportedDate());

                System.out.println(t.render());
            }
        }
        if (found == 0 ) {
            MyTable.info(id + " Product Not Found!");
            return;
        }
    }
}
