package Crud;

import Constants.MyTable;
import database.ConnectionDB;
import homepage.Product;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class SaveProduct {
    static Connection  con = ConnectionDB.getConnection();
    static PreparedStatement preStmt = null;
    static ArrayList<Integer> listDeteted = DeleteProduct.getListDeleted();
    public static void saveData(ArrayList<Product> products){
        try {
            updateData(products);
            deleteProduct();
            addProduct(products);
            MyTable.info("Product Save Successfully");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    static void updateData(ArrayList<Product> products) throws SQLException{
        for (Product product : products){
            if (product.getStatus().equals("isUpdate")){
                preStmt = con.prepareStatement("Update tblproduct set name = ?, unitprice = ?, quantity = ? where id = ?");
                preStmt.setString(1, product.getName());
                preStmt.setDouble(2,product.getUnitPrice());
                preStmt.setInt(3,product.getStockQty());
                preStmt.setInt(4,product.getId());
                preStmt.executeUpdate();
                product.setStatus("active");
            }
        }
    }
    static void deleteProduct() throws SQLException{
        for (int list : listDeteted){
            preStmt = con.prepareStatement("delete from tblproduct where id = ? ");
            preStmt.setInt(1,list);
            preStmt.executeUpdate();
        }
    }
    static void addProduct(ArrayList<Product> products) throws SQLException{
        for (Product product : products){
            if (product.getStatus().equals("isAdded")){
                preStmt = con.prepareStatement("INSERT INTO tblproduct (\"name\", \"unitprice\", \"quantity\", \"importdate\") VALUES (?,?,?,?)");
                preStmt.setString(1,product.getName());
                preStmt.setDouble(2, product.getUnitPrice());
                preStmt.setInt(3,product.getStockQty());
                preStmt.setString(4,product.getImportedDate());
                preStmt.executeUpdate();
                product.setStatus("active");
            }
        }
    }
}
