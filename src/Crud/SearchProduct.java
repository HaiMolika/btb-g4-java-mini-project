package Crud;

import homepage.Main;
import homepage.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

public class SearchProduct {
    static Scanner sc = new Scanner(System.in);
    static ArrayList<Product> searchList = new ArrayList();
    public static void SearchProduct(ArrayList<Product> products,int row,int pageStand){
        System.out.print("Search by Name : ");
        String name = sc.nextLine();
        for (Product product : products){
            if (product.getName().toUpperCase().contains(name.toUpperCase())){
                searchList.add(product);
            }
        }
        searchMenu(searchList, row, pageStand);
        searchList.clear();
    }
    public static void searchMenu(ArrayList<Product> searchList, int row, int pageStand){
        String option;
        Main.display(searchList,row,pageStand);
        while (true){
            Table tb = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
            tb.addCell(" F)irst  | P)revious   | N)ext   | L)ast   | B)ack  ");
            System.out.println(tb.render());
            System.out.print("Command --> ");
            option = sc.nextLine();
            option.toLowerCase();
            switch (option){
                case "f" :
                    pageStand = Main.gotoFirstPage();
                    Main.display(searchList, row, pageStand);
                    break;
                case "p" :
                    pageStand = Main.gotoPrevious(searchList, row, pageStand);
                    Main.display(searchList, row, pageStand);
                    break;
                case "n" :
                    pageStand = Main.gotoNext(searchList, row, pageStand);
                    Main.display(searchList, row, pageStand);
                    break;
                case "l" :
                    pageStand = Main.gotoLastPage(searchList, row);
                    Main.display(searchList, row, pageStand);
                    break;
                case "b" :
                    return;
                default:
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    System.out.println("Please input Valid Option.");
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~");
            }
        }
    }

}
