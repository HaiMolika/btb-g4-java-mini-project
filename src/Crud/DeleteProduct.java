package Crud;

import Constants.*;
import Validator.Validator;
import homepage.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.Scanner;

public class DeleteProduct {
    static Scanner sc = new Scanner(System.in);
    static ArrayList<Integer> listDeleted = new ArrayList<>();
    public static ArrayList getListDeleted(){
        return listDeleted;
    }
    public static void DeleteProduct(ArrayList<Product> products){
        Table tdisplay = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        tdisplay.setColumnWidth(0,30,30);
        int id =Validator.getInteger(sc,"Please Input ID of Product : ");
        int found = 0;
        for (Product product : products){
            if (product.getId() == id){
                found++;
                tdisplay.addCell("ID          : " + product.getId());
                tdisplay.addCell("Name        : " + product.getName());
                tdisplay.addCell("Unit Price  : " + product.getUnitPrice());
                tdisplay.addCell("Qty         : " + product.getStockQty());
                tdisplay.addCell("Import Date : " + product.getImportedDate());
                System.out.println(tdisplay.render());
                System.out.println();
                System.out.println(Constants.IS_DELETE_RECORD);
                String isDelete = sc.nextLine();
                while(isDelete.equals("Y") || isDelete.equals("y")){
                    products.remove(product);
                    listDeleted.add(id);
                    MyTable.info("Product was Removed");
                    return;
                }
                MyTable.info("Product Not Remove!");
            }
        }
        if (found == 0 ) {
            MyTable.info(id + " Product Not Found!");
            return;
        }
    }
}
