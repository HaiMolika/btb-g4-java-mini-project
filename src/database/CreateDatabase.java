package database;

import java.sql.*;

public class CreateDatabase {
    private static Connection con = null;
    private static Statement stmt = null;
    //Connection Username and Password
    static final String USER = "postgres";
    static final String PASS = "postgres";

    private static String url = "jdbc:postgresql://localhost:5432/";
    private static String driverName = "org.postgresql.Driver";
    private static String DB_NAME = "product_db";
    private static String DB_TABLE = "tblproduct";
    public CreateDatabase(){
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url,USER,PASS);
            stmt = con.createStatement();

            String sql = "SELECT EXISTS(SELECT datname FROM pg_catalog.pg_database WHERE datname = '"+DB_NAME+"')";

            ResultSet isExists = stmt.executeQuery(sql);
            isExists.next();

            if (isExists.getString(1).equals("f")){
                stmt.executeUpdate("create database "+DB_NAME);
                createTable(DB_TABLE);
            }else{
                 createTable(DB_TABLE);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            try {
                con.close();
                stmt.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }

    }
    static void createTable(String table){
        try{
            Connection con = ConnectionDB.getConnection();
            Statement stmt = con.createStatement();

            DatabaseMetaData dbm = con.getMetaData();
            ResultSet rs = dbm.getTables(null,null,table,null);
            if (rs.next()){
                System.out.println("Table already exist");
            }else{
                String sql="CREATE TABLE \"public\".\""+ table +"\" (\n" +
                        "  \"id\" integer NOT NULL GENERATED ALWAYS AS IDENTITY,\n" +
                        "  \"name\" varchar(30),\n" +
                        "  \"unitprice\" numeric(10,2),\n" +
                        "  \"quantity\" integer ,\n" +
                        "  \"importdate\" varchar(30),\n" +
                        "  PRIMARY KEY (\"id\")\n" +
                        ")\n" +
                        ";";
                stmt.executeUpdate(sql);
                System.out.println("success");
            }
        }catch (Exception e){
           e.printStackTrace();
        }finally {
            try {
                stmt.close();
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

}
