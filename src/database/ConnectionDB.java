package database;

import java.sql.*;

public class ConnectionDB {

    //Connection Username and Password
    static final String USER = "postgres";
    static final String PASS = "postgres";

    private static String url = "jdbc:postgresql://localhost:5432/product_db";
    private static String driverName = "org.postgresql.Driver";
    private static Connection con;

    public static Connection getConnection() {
        try {
            Class.forName(driverName);
            try {
                con = DriverManager.getConnection(url,USER, PASS);
            } catch (SQLException ex) {
                System.out.println("Failed to create the database connection.");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver not found.");
        }
        return con;
    }
}