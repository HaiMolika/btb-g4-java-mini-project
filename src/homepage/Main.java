package homepage;

import Constants.MyTable;
import Crud.*;
import Validator.Validator;
import database.ConnectionDB;
import database.CreateDatabase;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main{
    static Scanner scanner = new Scanner(System.in);
    static Connection con = null;
    static Statement st = null;
    static ResultSet rs = null;
    static int id = 0;
    Main(){
        new CreateDatabase();
    }
    public static void main(String[] args) {
        new Main();
        ArrayList<Product> listStore = new ArrayList<>();
        String option ="";
        int row = 3, pageStand =1;
        print();
        loadData(listStore);
        while(true){
            menu();
            System.out.print("Command --> ");
            option = scanner.nextLine();
            option = option.toLowerCase();
            switch (option){
                case "*" :      //Display
                    display(listStore,row, pageStand);
                    break;
                case "w" :       //Write
                    try {
                        AddProduct.addProduct(listStore);
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    break;
                case "r" :       //Read
                    int id = Validator.getInteger(scanner,"Please Input ID of Product : ");
                    ReadProduct.read(listStore,id);
                    break;
                case "u" :       //Update
                    UpdateProduct.update(listStore);
                    break;
                case "d" :       //Delete
                    DeleteProduct.DeleteProduct(listStore);
                    break;
                case "f" :       //First
                    pageStand = gotoFirstPage();
                    display(listStore, row, pageStand);
                    break;
                case "p" :        //Previous
                    pageStand = gotoPrevious(listStore, row, pageStand);
                    display(listStore, row, pageStand);
                    break;
                case "n" :       //Next
                    pageStand = gotoNext(listStore, row, pageStand);
                    display(listStore, row, pageStand);
                    break;
                case "l" :       //Last
                    pageStand = gotoLastPage(listStore,row);
                    display(listStore, row, pageStand);
                    break;
                case "s" :       //Search
                    SearchProduct.SearchProduct(listStore,row,1);
                    break;
                case "g" :       //Goto
                    pageStand = gotoSpecificPage(listStore, row);
                    display(listStore, row, pageStand);
                    break;
                case "se" :     //Set row
                    row = setRow();
                    pageStand = gotoFirstPage();
                    scanner.nextLine();
                    break;
                case "sa" :     //Save
                    SaveProduct.saveData(listStore);
                    break;
                case "ba" :      //Back up
                    break;
                case "re" :     //Restore
                    break;
                case "h" :       //Help
                    getHelp();
                    break;
                case "_10m":
                    InsertProduct.insertProduct();
                    System.out.println("Total READ Time : ");
                    Main.loadData(listStore);
                    break;
                case "e" :       //Exit
                    System.out.println("Good Bye ^,^");
                    System.exit(0);
                default :
                    MyTable.info("Please input VALID option.");
            }
        }
    }
    public static void menu(){
        System.out.println();
        Table tbl = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        tbl.addCell(" *)Display    | W)rite    | R)ead     | U)pdate   | D)elete    | F)irst    | P)revious  | N)ext   | L)ast ");
        tbl.addCell(" S)earch      | G)oto     | Se)t row  | Sa)ve     | Ba)ck up   | Re)store  | H)elp      | E)xit");
        System.out.println(tbl.render());
    }
    public static void print(){
        TitlePrinting tp1 = new TitlePrinting("\t\t\t\t\t\t\t\t\t\t\t\t\tWelcome to\n", 10);
        TitlePrinting tp2 = new TitlePrinting("\t\t\t\t\t\t\t\t\t\t\t\t Stock Management ", 10);
        TitlePrinting tp3 = new TitlePrinting("\n" +
                "                \t\t\t ___   _ _____ _____ _   __  __ ___   _   _  _  ___  ___ _ _ \n" +
                "                \t\t\t| _ ) /_\\_   _|_   _/_\\ |  \\/  | _ ) /_\\ | \\| |/ __|/ __| | | \n" +
                "                \t\t\t| _ \\/ _ \\| |   | |/ _ \\| |\\/| | _ \\/ _ \\| .` | (_ | (_ |_  _|\n" +
                "                \t\t\t|___/_/ \\_\\_|   |_/_/ \\_\\_|  |_|___/_/ \\_\\_|\\_|\\___|\\___| |_| \n" +
                "                                                                           \n", 1);
        TitlePrinting tp4 = new TitlePrinting("Please wait loading...\n", 30);
        TitlePrinting tp5 = new TitlePrinting("Current time loading : ", 30);
        try {
            tp1.start();
            tp1.join();
            tp2.start();
            tp2.join();
            tp3.start();
            tp3.join();
            tp4.start();
            tp4.join();
            tp5.start();
            tp5.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void loadData(List<Product> listStore){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        long time1 = new Date().getTime();
        try {
            con = ConnectionDB.getConnection();
            con = ConnectionDB.getConnection();
            st = con.createStatement();
            rs = st.executeQuery("select * from tblproduct order by id");
            while (rs.next()){
                listStore.add(new Product(rs.getInt(1), rs.getString(2),rs.getDouble("unitprice"),rs.getInt(4), rs.getString(5),"active"));
                id = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                con.close();
                st.close();
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        long time2 = new Date().getTime();
        long milli = time2 - time1;
        TitlePrinting tp6 = new TitlePrinting(milli +"", 10);
        tp6.start();
        try {
            tp6.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void display(List<Product> listStore, int row, int pageStand){
        int totalPage=1, limitRow=0, initRow=0, limit=0,totalRecord= listStore.size();
        if(listStore.size()>=row){
            if(row*pageStand<listStore.size() || row*pageStand>listStore.size()){
                totalPage =listStore.size()/row +1;
                if(listStore.size()%row !=0){
                    //the last page is not full
                    if(pageStand == totalPage){
                        limit = listStore.size() - (totalPage-1)*row;
                        limitRow = listStore.size();
                        initRow = limitRow - limit;
                    }
                    else {
                        limitRow = pageStand*row;
                        initRow = limitRow - row;
                    }
                }
                else {
                    //the last page is full
                    totalPage = listStore.size()/row;
                    limitRow = pageStand*row;
                    initRow = limitRow - row;
                }
            }else {
                totalPage =listStore.size()/row;
                limit = listStore.size() - (totalPage-1)*row;
                limitRow = listStore.size();
                initRow = limitRow - limit;
            }
        }else{
            limit = listStore.size();
            limitRow = limit;
            initRow = limitRow - limit;
        }
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        CellStyle style = new CellStyle(CellStyle.HorizontalAlign.center);
        t.setColumnWidth(0,10,10);
        t.setColumnWidth(1,20,25);
        t.setColumnWidth(2,15,25);
        t.setColumnWidth(3,15,15);
        t.setColumnWidth(4,21,21);
        t.addCell("ID",style);
        t.addCell("NAME",style);
        t.addCell("Unit Price", style);
        t.addCell("Qty",style);
        t.addCell("Imported Date", style);
        for(int i=initRow; i<limitRow; i++){
            t.addCell(listStore.get(i).getId() +"", style);
            t.addCell(listStore.get(i).getName());
            t.addCell(String.valueOf(listStore.get(i).getUnitPrice()), style);
            t.addCell(String.valueOf(listStore.get(i).getStockQty()), style);
            t.addCell(listStore.get(i).getImportedDate(), style);
        }
        System.out.println(t.render());
        Table bottompage = new Table(5, BorderStyle.DESIGN_CURTAIN, ShownBorders.SURROUND_HEADER_FOOTER_AND_COLUMNS);
        bottompage.setColumnWidth(0,20,20);
        bottompage.setColumnWidth(1,10,10);
        bottompage.setColumnWidth(2,15,25);
        bottompage.setColumnWidth(3,15,15);
        bottompage.setColumnWidth(4,19,19);
        String word = "Page " + pageStand + " of " + totalPage;
        bottompage.addCell(word, 2);
        bottompage.addCell("", 2);
        bottompage.addCell("Total Record: " + listStore.size());
        System.out.println(bottompage.render());
    }
    public static int setRow(){
        int row =3;
        System.out.print("Input row to display : ");
        row = scanner.nextInt();
        MyTable.info(" Set row to "+row+" Successfully.");
        return row;
    }
    public static int gotoFirstPage(){
        return 1;
    }
    public static int gotoLastPage(List<Product> listStore, int row){
        int pageStand = 1;
        if(listStore.size()%row != 0){
            pageStand = listStore.size()/row + 1;
        }
        else{
            pageStand = listStore.size()/row;
        }
        return pageStand;
    }
    public static int gotoPrevious(List<Product> listStore, int row, int pageStand){
        if(pageStand != 1)
            return pageStand - 1;
        else
            return gotoLastPage(listStore, row);
    }
    public static int gotoNext(List<Product> listStore, int row, int pageStand){
        if(listStore.size()%row != 0){
            if(pageStand >= (int) listStore.size()/row + 1)
                return gotoFirstPage();
            else
                return pageStand + 1;
        }
        else {
            if(pageStand >= (int) listStore.size()/row)
                return gotoFirstPage();
            else
                return pageStand + 1;
        }
    }
    public static int gotoSpecificPage(List<Product> listStore, int row){
        int totalPage;
        int inputRow;
        System.out.print("Go to page : ");
        inputRow = scanner.nextInt();
        scanner.nextLine();
        if(listStore.size()%row != 0){
            totalPage = (int)listStore.size()/row + 1;
        }
        else {
            totalPage = (int)listStore.size()/row;
        }
        if(inputRow>0 && inputRow <= totalPage){
            return inputRow;
        }
        else{
            System.out.println(inputRow + " is not Available.");
            return 1;
        }
    }
    static void getHelp(){
        System.out.println("+----------------------------------------------------------------------------------------+");
        System.out.println("! 1.\tPress\t* : Display all record of products                                       !");
        System.out.println("! 2.\tPress\tw : Add new product                                                      !");
        System.out.println("!   \tPress\tw#proname-unitprice-qty : shortcut for add new product                   !");
        System.out.println("! 3.\tPress\tr : read Content any content                                             !");
        System.out.println("!   \tPress\tr#proId : shortcut for read product by Id                                !");
        System.out.println("! 4.\tPress\tu : Update Data                                                          !");
        System.out.println("! 5.\tPress\td : Delete Date                                                          !");
        System.out.println("!   \tPress\td#ProId : shortcut for delete product by Id                              !");
        System.out.println("! 6.\tPress\tf : Display First Page                                                   !");
        System.out.println("! 7.\tPress\tp : Display Previous Page                                                !");
        System.out.println("! 8.\tPress\tn : Display Next Page                                                    !");
        System.out.println("! 9.\tPress\tl : Display Previous Page                                                !");
        System.out.println("! 10.\tPress\ts : Search product by name                                               !");
        System.out.println("! 11.\tPress\tsa : Save record to file                                                 !");
        System.out.println("! 12.\tPress\tba : Backup data                                                         !");
        System.out.println("! 13.\tPress\tre : Restore data                                                        !");
        System.out.println("! 14.\tPress\th : Help                                                                 !");
        System.out.println("+----------------------------------------------------------------------------------------+");
    }

}
