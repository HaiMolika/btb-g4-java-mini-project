package homepage;

public class Product {
    private int id, stockQty;
    private String name, importedDate;
    private double unitPrice;
    private String status;
    public Product(int id, String name, double unitPrice, int stockQty, String importedDate,String status) {
        this.id = id;
        this.stockQty = stockQty;
        this.name = name;
        this.importedDate = importedDate;
        this.unitPrice = unitPrice;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public int getStockQty() {
        return stockQty;
    }

    public String getName() {
        return name;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public double getUnitPrice() {
        return unitPrice;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStockQty(int stockQty) {
        this.stockQty = stockQty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
}