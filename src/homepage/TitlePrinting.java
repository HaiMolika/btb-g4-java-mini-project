package homepage;

class TitlePrinting extends Thread{
    String word;
    int time;
    TitlePrinting(String word, int time){
        this.word = word;
        this.time = time;
    }
    static void print(String word, int time){
        for (int i=0; i<word.length(); i++){
            System.out.print(word.charAt(i));
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void run() {
        print(word, time);
    }
}
